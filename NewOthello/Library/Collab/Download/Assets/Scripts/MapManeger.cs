﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapManeger : MonoBehaviour
{

    private enum Piece_Color
    {
        EMPTY,
        BLACK,
        WHITE,
        Effect = 10
    }

    private delegate int Obj_Makes(int x, int y, int value);

    [SerializeField] private GameObject[] Piecetype = default;
    [SerializeField] private GameObject[] Parent = default;

    [SerializeField] private Text judge_text = default;

    [SerializeField] private Text debug_text = default;
    [SerializeField] private Text debug_text2 = default;
    [SerializeField] private Text debug_text3 = default;


    private int[,,,] Upset_num = default;
    private int[,] Now_Tiles = default;

    public static int set_piece_coler = default;

    public static bool Piece_set_ok = false;

    private RaycastHit2D Hitobj = default;
    private int Hit_X = default;
    private int Hit_Y = default;

    private int MAP_X = default;
    private int MAP_Y = default;

    // Start is called before the first frame update

    private void Start()
    {
        //Othello_Start_Map(1);
        //Piece_Set_Check(1);

    }


    // Update is called once per frame
    void Update()
    {
        debug_text2.text = "" + set_piece_coler;

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log(set_piece_coler);
            PieceSeting();
            PieceSet(set_piece_coler);
        }
        if (Piece_set_ok == true)
        {
            Map_Reset();
        }
    }


    private void PieceSeting()
    {


        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics2D.Raycast((Vector2)ray.origin, (Vector2)ray.direction))
        {
            Hitobj = Physics2D.Raycast((Vector2)ray.origin, (Vector2)ray.direction);


            int X = (int)Hitobj.collider.gameObject.transform.position.x;
            int Y = (int)Hitobj.collider.gameObject.transform.position.y;
            if (Hit_X >= 0 && Hit_X <= MAP_X && Hit_Y >= 0 && Hit_Y <= MAP_Y)
            {
                Hit_X = X;
                Hit_Y = Y;
            }

        }

    }

    private void Piece_Make(int x, int y)
    {
        if (Now_Tiles[x, y] != (int)Piece_Color.EMPTY)
        {
            Makes(x, y);
        }
    }

    private void PieceSet(int value)
    {

        debug_text3.text = "{" + Hit_X + "}{" + Hit_Y + "}";

        if (Now_Tiles[Hit_X, Hit_Y] == (int)Piece_Color.Effect)
        {
            for (int i = -1; i < 2; i++)
            {
                for (int j = -1; j < 2; j++)
                {
                    int z = Upset_num[Hit_X, Hit_Y, i + 1, j + 1];

                    if (i == 0 && j == 0)
                    {
                        continue;
                    }
                    else
                    {
                        Now_Tiles[Hit_X, Hit_Y] = value;
                        Piece_set_ok = true;

                        while (z >= 2){
                            Now_Tiles[Hit_X + i * (z - 1), Hit_Y + j * (z - 1)] = value;
                        z--;
                        }
                    }
                }
            }
        }
        else
        {
            judge_text.text = "そこには置けないよ！！";
        }

        Invoke("Reset_Text", 1);


    }

    public void Piece_Set_Check(int value)
    {
        Upset_num = new int[Now_Tiles.GetLength(0),Now_Tiles.GetLength(1),3,3];

        for (int x = 0; x < Now_Tiles.GetLength(0) - 1; x++)
        {
            for (int y = 0; y < Now_Tiles.GetLength(1) - 1; y++)
            {
                for (int i = -1; i < 2; i++)
                {
                    for (int j = -1; j < 2; j++)
                    {
                        Upset_num[x, y, i + 1, j + 1] = 0;

                        if (i == 0 && j == 0)
                        {
                            continue;
                        }
                        else
                        {
                            int z = 2;
                            if (x - 1 >= Hit_X + i && y - 1 >= Hit_Y + j && Hit_X + i >= 0 && Hit_Y + j >= 0)
                            {
                                if (Now_Tiles[x, y] == (int)Piece_Color.EMPTY && Now_Tiles[x + i, y + j] != (int)Piece_Color.EMPTY && Now_Tiles[x + i, y + j] != value)
                                {
                                    while ((Now_Tiles.GetLength(0) >= x + i * z && Now_Tiles.GetLength(1) >= y + j * z) && Now_Tiles[x + i * z, y + j * z] != (int)Piece_Color.EMPTY
                                         && x + i * z >= 0 && y + j * z >= 0)
                                    {
                                        if (value == Now_Tiles[x + i * z, y + j * z])
                                        {
                                            Now_Tiles[x, y] = (int)Piece_Color.Effect;
                                            Effect_Make(x, y);

                                            Upset_num[x, y, i + 1, j + 1] = z;


                                            break;
                                        }
                                        z++;
                                    }
                                }
                            }
                        }
                    }

                }
            }
        }
    }

    private void Reset_Text()
    {
        judge_text.text = "JUDGE";
    }

    private void Map_Reset()
    {
        //駒とエフェクトの削除
        foreach (Transform child in Parent[1].gameObject.transform)
        {
            Destroy(child.gameObject);
        }
        foreach (Transform child in Parent[2].gameObject.transform)
        {
            Destroy(child.gameObject);
        }

        debug_text.text = "";

        for (int x = 0; x < MAP_X; x++)
        {
            for (int y = 0; y < MAP_Y; y++)
            {
                if( Now_Tiles[x,y] == (int)Piece_Color.Effect)
                {
                    Now_Tiles[x, y] = 0;
                }
                Piece_Make(x, y);

            }
        }

        for (int j = MAP_Y - 1; j >= 0; j--)
        {
            for (int i = 0; i < MAP_X; i++)
            {
                debug_text.text +=  Now_Tiles[i,j] + " " ;
            }
            debug_text.text += "\n";
        }

    }

    private void Map_Make(int x, int y)
    {
        Now_Tiles = new int[x, y];

        for (int i = 0; i < MAP_X; i++)
        {
            for (int j = 0; j < MAP_Y; j++)
            {
                Now_Tiles[i, j] = (int)Piece_Color.EMPTY;
                Makes(i, j);
            }
        }

    }


    public void Othello_Start_Map(int Select)
    {
        switch (Select)
        {
            case 1:
                MAP_X = 8;
                MAP_Y = 8;
                Map_Make(MAP_X, MAP_Y);
                Othello_Start_Map_01();
                break;
        }
        Map_Reset();
    }

    private void Othello_Start_Map_01()
    {
        Now_Tiles[3, 4] = (int)Piece_Color.BLACK;
        Now_Tiles[4, 3] = (int)Piece_Color.BLACK;
        Now_Tiles[3, 3] = (int)Piece_Color.WHITE;
        Now_Tiles[4, 4] = (int)Piece_Color.WHITE;
    }

    private void Effect_Make(int x, int y)
    {
        Makes(x, y); 
    }

    private void Makes(int x, int y)
    {
        int makepiece = default;
        GameObject Obj = Instantiate(Piecetype[Now_Tiles[x, y]], new Vector3(x, y, 10), new Quaternion(0, 0, 0, 0));
        switch( Now_Tiles[x,y])
        {
            case 0:
                makepiece = 0;
                break;

            case 1:
            case 2:
                makepiece = 1;
                break;

            case 10:
                makepiece = 2;
                break;

        }
        Obj.transform.parent = Parent[makepiece].transform;

    }


}



