﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameMneger : MonoBehaviour
{
    public static int Map_select = default;

    [SerializeField] GameObject _mapmanegerObj = default;
    [SerializeField] GameObject _pieces = default;
    private MapManeger _mapmaneger;


    private void Awake()
    {
        _mapmaneger = _mapmanegerObj.GetComponent<MapManeger>();
        Map_select = 1;
    }

    private void Start()
    {
        StartCoroutine(GameLoop());
    }

    private IEnumerator GameLoop()
    {

        Map_select = 1;

        yield return StartCoroutine(GameStart());
        yield return StartCoroutine(GamePlay());
        yield return StartCoroutine(GameSet());

        yield return null;
    }

    private IEnumerator GamePlay()
    {
    

            yield return StartCoroutine(Player1());
            yield return StartCoroutine(Player2());



    }

    private IEnumerator Player1()
    {
        MapManeger.Piece_set_ok = false;
        MapManeger.set_piece_coler = 1;
        _mapmaneger.Piece_Set_Check(MapManeger.set_piece_coler);
        while(MapManeger.Piece_set_ok)
        {
            Debug.Log("ターンエンド");
            yield return null;
        }
    }
    private IEnumerator Player2()
    {
        MapManeger.Piece_set_ok = false;
        MapManeger.set_piece_coler = 2;
        _mapmaneger.Piece_Set_Check(MapManeger.set_piece_coler);
        while (MapManeger.Piece_set_ok)
        {
            Debug.Log("ターンエンド");
            yield return null;
        }


    }


    private IEnumerator GameStart()
    {
        GameEnabled();
        _mapmaneger.Othello_Start_Map(Map_select);

        yield return null;
    }

    private IEnumerator GameSet()
    {
        yield return null;
    }

    private void GameDisabled()
    {
        _mapmaneger.enabled = false;
    }

    private void GameEnabled()
    {
        _mapmaneger.enabled = true;
    }

}
