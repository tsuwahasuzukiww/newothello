﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMangeger : MonoBehaviour
{
    [SerializeField] private Text _now_turn_text = default;
    [SerializeField] private Text _judge_text = default;
    [SerializeField] private Text _gameset_text = default;
    [SerializeField] private Text[] _team_text = default;
    [SerializeField] private Text[] _value_text = default;
    [SerializeField] private Text _winer_text = default;
    [SerializeField] private Text _winer_team_text = default;
    [SerializeField] private Image _winer_image = default;
    [SerializeField] private Image _info_image = default;
    [SerializeField] private GameObject _1vs1_Obj = default;
    [SerializeField] private GameObject _2vs2_Obj = default;
    [SerializeField] private Image _effect_image = default;
    [SerializeField] private Image _now_turn_image = default;
    [SerializeField] private Sprite[] _now_trun_sprite = default;
    [SerializeField] private Text _user_name = default;
    [SerializeField] private Text _user_rank = default;
    [SerializeField] private Text _user_id = default;
    [SerializeField] private Text _user_notlogin = default;
    [SerializeField] private Text _user_notlogin2 = default;




    Vector2 Effect_posi = default;


    public void Judge()
    {
        _judge_text.text = "そこには置けないよ！！";

    }

    public void Judge_Reset()
    {
        _judge_text.text = "";

    }

    public void Player_turn(int playernum, int turn_num)
    {
        string turn_str = "";

        switch (playernum)
        {
            case 1:
                turn_str = "Black";
                Black_Turn_Info();
                break;
            case 2:
                turn_str = "White";
                White_Turn_Info();
                break;
            case 3:
                //黒
                turn_str = "Lead";
                Black_Turn_Info();
                break;
            case 4:
                //白
                turn_str = "Lead";
                White_Turn_Info();
                break;
            case 5:
                //黒
                turn_str = "Support";
                Black_Turn_Info();
                break;
            case 6:
                //白
                turn_str = "Support";
                White_Turn_Info();
                break;

        }

        if (playernum <= 2)
        {
            _1vs1_Obj.SetActive(true);
            _effect_image.transform.localPosition = new Vector2(-150 + 300 * (playernum - 1), Effect_posi.y);
        }
        else if (playernum >= 3)
        {
            _2vs2_Obj.SetActive(true);
            _effect_image.transform.localPosition = new Vector2(-350 + 100 * (turn_num - 1), Effect_posi.y);
        }

        _now_turn_image.sprite = _now_trun_sprite[playernum - 1];
        _now_turn_text.text = "Player : " + turn_str;

    }

    public void Black_Turn_Info()
    {

        _info_image.color = Color.black;
        _now_turn_text.color = Color.white;
        _judge_text.color = Color.white;
        _gameset_text.color = Color.white;

    }

    public void White_Turn_Info()
    {
        _info_image.color = Color.white;
        _now_turn_text.color = Color.black;
        _judge_text.color = Color.black;
        _gameset_text.color = Color.black;

    }


    public void Team_Set()
    {
        _team_text[0].text = "Black";
        _team_text[1].text = "White";

    }

    public void Effect_Set()
    {
        Effect_posi = _effect_image.transform.localPosition;
    }

    public void Piece_Count(int BP, int WP)
    {
        _value_text[0].text = "" + BP;
        _value_text[1].text = "" + WP;
    }

    public void Piece_Count_Secret()
    {
        _value_text[0].text = "???";
        _value_text[1].text = "???";
    }

    public void GameSet()
    {
        _judge_text.enabled = false;
        _now_turn_text.enabled = false;
        _now_turn_image.enabled = false;
        _gameset_text.enabled = true;
    }

    public void Winer(string winer_team,Color winer_color)
    { 
        if (winer_color == Color.black)
        {
            _winer_image.color = winer_color;
            _winer_text.color = Color.white;
            _winer_team_text.color = Color.white;
        }
        else if (winer_color == Color.white)
        {
            _winer_image.color = winer_color;
            _winer_text.color = Color.black;
            _winer_team_text.color = Color.black;
        }
        _winer_team_text.text = "" + winer_team;
    }

    public void TitleUI()
    {

        if (AcountManeger.MyUserName == null)
        {
            _user_name.enabled = false;
            _user_rank.enabled = false;
            _user_id.enabled = false;
            _user_notlogin.enabled = true;
            _user_notlogin2.enabled = true;

        }

        else
        {
            _user_name.enabled = true;
            _user_rank.enabled = true;
            _user_id.enabled = true;
            _user_notlogin.enabled = false;
            _user_notlogin2.enabled = false;
            _user_name.text = "Name : " + AcountManeger.MyUserName;
            _user_rank.text = "Rank : " + AcountManeger.MyUserRank;
            _user_id.text = "ID : " + AcountManeger.MYUserId;

        }


    }
}
