﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResultManeger : MonoBehaviour
{
   [SerializeField] UIMangeger _uimaneger;
   
    private void Start()
    {
        _uimaneger.Team_Set();
        _uimaneger.Piece_Count(GameMneger.Black_Piece_Value,GameMneger.White_Piece_Value);
        _uimaneger.Winer(GameMneger.Winer_Team,GameMneger.Winer_Color);
    }

}
