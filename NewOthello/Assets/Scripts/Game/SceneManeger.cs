﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManeger : MonoBehaviour
{
    [SerializeField] TitleManeger _titleManeger = default;

    public void OffLine_1vs1()
    {
        _titleManeger.OffLine_1vs1();
        MainScene();
    }
    public void OffLine_2vs2()
    {
        _titleManeger.OffLine_2vs2();

        MainScene();
    }

    public void MainScene()
    {
        SceneManager.LoadScene("Main");
    }

    public void Title_Scene()
    {
        SceneManager.LoadScene("Title");
    }

    public void Result_Scene()
    {
        SceneManager.LoadScene("Result");
    }

    public void Login_Scene()
    {
        SceneManager.LoadScene("Login");
    }

}
