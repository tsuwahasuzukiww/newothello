﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleManeger : MonoBehaviour
{
    [SerializeField] UIMangeger _uimaneger = default;
    private void Start()
    {
        _uimaneger.TitleUI();
    }

    public void OffLine_1vs1()
    {
        GameMneger.Map_select = 1;
        GameMneger.Player_num_set = 2;
        GameMneger.Piece_color_set = 1;

    }
    public void OffLine_2vs2()
    {
        GameMneger.Map_select = 1;
        GameMneger.Player_num_set = 4;
        GameMneger.Piece_color_set = 3;
    }
}
