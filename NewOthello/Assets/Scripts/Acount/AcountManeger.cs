﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System; // File
using UnityEngine.Networking;


public class AcountManeger : MonoBehaviour
{

    [SerializeField] private InputField _inputName = default;
    [SerializeField] private InputField _inputPassWord = default;
    [SerializeField] private InputField _inputRePassWord = default;
    [SerializeField] private InputField _inputLoginName = default;
    [SerializeField] private InputField _inputLoginPassWord = default;
    [SerializeField] private InputField _inputLoginId = default;
    [SerializeField] private Text _result = default;
    [SerializeField] private Text _result2 = default;
    [SerializeField] private GameObject _Logincanvas = default;
    [SerializeField] private GameObject _NewLogincanvas = default;
    [SerializeField] private GameObject _LogOutcanvas = default;

    [SerializeField] private Text _user_name = default;
    [SerializeField] private Text _user_rank = default;
    [SerializeField] private Text _user_id = default;




    public static string MyUserName = default;
    public static long MyUserRank = default;
    public static long MYUserId = default;


    private List<AcounData> _acountList;


    private void Start()
    {
        // API を呼んだ際に想定されるレスポンス
        // [{"name":"\u3072\u3068\u308a\u3081","age":123,"hobby":"\u30b4\u30eb\u30d5"},{"name":"\u3075\u305f\u308a\u3081","age":25,"hobby":"walk"},{"name":"\u3055\u3093\u306b\u3093\u3081","age":77,"hobby":"\u5c71"}]
        //

        // Wwwを利用して json データ取得をリクエストする
 

        if (MyUserName != null)
        {
            _result.text = "ログイン";
            _result2.text = "しました！";
            _Logincanvas.SetActive(false);
            _NewLogincanvas.SetActive(false);
            _LogOutcanvas.SetActive(true);
            LoginUI();

        }
        else
        {
            _Logincanvas.SetActive(true);
            _NewLogincanvas.SetActive(true);
            _LogOutcanvas.SetActive(false);
        }

    }

    private void CallbackWebRequestSuccess(string response)
    {
        //Json の内容を MemberData型のリストとしてデコードする。
        _acountList = AcountDataModel.DeserializeFromJson(response);

        //memberList ここにデコードされたメンバーリストが格納される。
    }

    /// <summary>
    /// Callbacks the www failed.
    /// </summary>
    private void CallbackWebRequestFailed()
    {
        _result.text = "運営に報告";
        _result2.text = "してください！";
    }

    private IEnumerator DownloadJson(string id_num,Action<string> cbkSuccess = null, Action cbkFailed = null)
    {
        WWWForm form = new WWWForm();
        form.AddField("id", id_num);
        UnityWebRequest www = UnityWebRequest.Post("http://localhost/AcountApi/acount/getAcountdata",form);
        yield return www.SendWebRequest();
        if (www.error != null)
        {
            //レスポンスエラーの場合
            if (null != cbkFailed)
            {
                cbkFailed();
            }

        }
        else if (www.isDone)
        {
            // リクエスト成功の場合
            if (null != cbkSuccess)
            {
                cbkSuccess(www.downloadHandler.text);
            }



            yield return null;

        }
    }

    public void OnClickSetMassage()
    {
        SetJsonFromWWW();
    }

    private void SetJsonFromWWW()
    {

        string sTgtURL = "http://localhost/AcountApi/acount/setAcountdata/";

        string name = _inputName.text;
        string password = _inputPassWord.text;

        StartCoroutine(SetMassage(sTgtURL, name, password, CallbackWebRequestFailed));


    }

    private IEnumerator SetMassage(string url, string name, string password, Action cbkFaild = null)
    {

        if (_inputPassWord.text == _inputRePassWord.text)
        {

            WWWForm form = new WWWForm();
            form.AddField("name", name);
            form.AddField("password", password);

            UnityWebRequest webRequest = UnityWebRequest.Post(url, form);

            webRequest.timeout = 10;

            yield return webRequest.SendWebRequest();

            if (webRequest.error != null)
            {
                if (null != cbkFaild)
                {
                    cbkFaild();
                }
            }
            else if (webRequest.isDone)
            {


                StartCoroutine(LoginCol(webRequest.downloadHandler.text));
            }
        }
        else
        {
            _result.text = "パスワード";
            _result2.text = "が違います";
        }
    }

    public void Login()
    {

        StartCoroutine(LoginCol(_inputLoginId.text));

    }


    private IEnumerator LoginCol(string id_num)
    {
        _result.text = "読み込";
        _result2.text = "み中！";

        yield return StartCoroutine(
                   DownloadJson(
                       id_num,
                  CallbackWebRequestSuccess, // APIコールが成功した際に呼ばれる関数を指定
                  CallbackWebRequestFailed// APIコールが失敗した際に呼ばれる関数を指定
                   ));


        foreach (AcounData acountOne in _acountList)
        {

            if (acountOne.Name == _inputLoginName.text && acountOne.Password == _inputLoginPassWord.text && acountOne.ID == long.Parse(_inputLoginId.text))
            {
                MyUserName = acountOne.Name;
                MyUserRank = acountOne.Rank;
                MYUserId = acountOne.ID;
                _result.text = "ログイン";
                _result2.text = "しました！";
                _Logincanvas.SetActive(false);
                _NewLogincanvas.SetActive(false);
                _LogOutcanvas.SetActive(true);
                LoginUI();


            }
            else if (acountOne.Name == _inputName.text && acountOne.Password == _inputPassWord.text)
            {
                MyUserName = acountOne.Name;
                MyUserRank = acountOne.Rank;
                MYUserId = acountOne.ID;
                _result.text = "ログイン";
                _result2.text = "しました！";
                _Logincanvas.SetActive(false);
                _NewLogincanvas.SetActive(false);
                _LogOutcanvas.SetActive(true);
                LoginUI();

            }

        }

        if (_Logincanvas.activeSelf)
        {

            _result.text = "ログイン";
            _result2.text = "できません";

        }
        

    }

    public void LogOut()
    {
        MYUserId = default;
        MyUserName = null;
        MyUserRank = default;

        _result.text = "ログアウト";
        _result2.text = "しました！";


        _Logincanvas.SetActive(true);
        _NewLogincanvas.SetActive(true);
        _LogOutcanvas.SetActive(false);

    }


    public void LoginUI()
    {
        if (MyUserName == null)
        {
            _user_name.enabled = false;
            _user_rank.enabled = false;
            _user_id.enabled = false;

        }

        else
        {
            _user_name.enabled = true;
            _user_rank.enabled = true;
            _user_id.enabled = true;
            _user_name.text = "Name : " + MyUserName;
            _user_rank.text = "Rank : " + MyUserRank;
            _user_id.text = "ID : " + MYUserId;

        }


    }


}
