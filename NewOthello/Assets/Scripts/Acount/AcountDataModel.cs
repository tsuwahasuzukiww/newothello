﻿using System.Collections;
using System.Collections.Generic;
using MiniJSON;
using UnityEngine;

public class AcountDataModel
{

    public static List<AcounData> DeserializeFromJson(string sStrJson)
    {
        var ret = new List<AcounData>();

        // JSONデータは最初は配列から始まるので、Deserialize（デコード）した直後にリストへキャスト      
        IList jsonList = (IList)Json.Deserialize(sStrJson);

        // リストの内容はオブジェクトなので、辞書型の変数に一つ一つ代入しながら、処理
        foreach (IDictionary jsonOne in jsonList)
        {
            //新レコード解析開始

            var tmp = new AcounData();

            //該当するキー名が jsonOne に存在するか調べ、存在したら取得して変数に格納する。
            if (jsonOne.Contains("ID"))
            {
                tmp.ID = (long)jsonOne["ID"];
            }
            if (jsonOne.Contains("Name"))
            {
                tmp.Name = (string)jsonOne["Name"];
            }

            if (jsonOne.Contains("PassWord"))
            {
                tmp.Password = (string)jsonOne["PassWord"];
            }
            if (jsonOne.Contains("Rank"))
            {
                tmp.Rank = (long)jsonOne["Rank"];
            }

            //現レコード解析終了
            ret.Add(tmp);
        }

        return ret;
    }

}
