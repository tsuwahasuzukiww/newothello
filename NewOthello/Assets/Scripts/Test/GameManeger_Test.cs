﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameManeger_Test : MonoBehaviour
{

    [HideInInspector] public static int Map_select = default;
    [HideInInspector] public static int Player_num_set = default;

    [HideInInspector] public static int Black_Piece_Value = default;
    [HideInInspector] public static int White_Piece_Value = default;
    [HideInInspector] public static string Winer_Team = default;

    [SerializeField] GameObject _mapmanegerObj = default;
    private MapManger_Test _mapmaneger;


    private void Awake()
    {
        _mapmaneger = _mapmanegerObj.GetComponent<MapManger_Test>();
    }

    private void Start()
    {
        StartCoroutine(GameLoop());
    }

    private IEnumerator GameLoop()
    {

        Map_select = 1;
        _mapmaneger.Player_num = Player_num_set;
        _mapmaneger.set_piece_coler = 1;

        yield return StartCoroutine(GameStart());
        yield return StartCoroutine(GameSet());

        yield return null;
    }

    private IEnumerator GameStart()
    {
        GameEnabled();
        _mapmaneger.Othello_Start_Map(Map_select);

        while (!_mapmaneger.Game_Set)
        {
            yield return null;
        }
    }

    private IEnumerator GameSet()
    {
        Debug.Log("GameLock");
        _mapmaneger.enabled = false;
        _mapmaneger.Game_Lock = true;
        Black_Piece_Value = _mapmaneger.Black_Piece_Value;
        White_Piece_Value = _mapmaneger.White_Piece_Value;

        if (Black_Piece_Value >= White_Piece_Value && Player_num_set == 2)
        {
            Winer_Team = "Black !";
        }
        else if (White_Piece_Value >= Black_Piece_Value && Player_num_set == 2)
        {
            Winer_Team = "White !";
        }
        else if (Black_Piece_Value >= White_Piece_Value && Player_num_set == 4)
        {
            Winer_Team = "Black Team !";
        }
        else if (White_Piece_Value >= Black_Piece_Value && Player_num_set == 4)
        {
            Winer_Team = "White Team !";
        }

        yield return new WaitForSeconds(3);
        SceneManager.LoadScene("Result");
        yield return null;
    }

    private void GameEnabled()
    {
        _mapmaneger.enabled = true;
    }

}

