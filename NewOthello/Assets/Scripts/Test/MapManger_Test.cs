﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapManger_Test : MonoBehaviour
{

    private enum Piece_Color
    {
        EMPTY,
        BLACK,
        WHITE,
        BLACK_L,
        WHITE_L,
        BLACK_S,
        WHITE_S,
        EFFECT = 10
    }

    private delegate int Obj_Makes(int x, int y, int value);

    [SerializeField] private GameObject[] Piecetype = default;
    [SerializeField] private GameObject[] Parent = default;

    [SerializeField] private Text debug_text = default;
    [SerializeField] private Text debug_text2 = default;
    [SerializeField] private Text debug_text3 = default;

    [SerializeField] private UIMangeger _uimaneger;

    private int[,,,] Upset_num = default;
    private int[,] Now_Tiles = default;

    [HideInInspector] public int set_piece_coler = default;

    [HideInInspector] public int Player_num = default;
    [HideInInspector] public int Black_Piece_Value = default;
    [HideInInspector] public int White_Piece_Value = default;

    [HideInInspector] public bool Game_Lock = false;

    [HideInInspector] public bool Game_Set = false;

    private const int Secret_num = 15;

    private int Pass_num = default;
    private int Trun_num = 1;

    private bool Piece_set_ok = false;

    private RaycastHit2D Hitobj = default;
    private int Hit_X = default;
    private int Hit_Y = default;

    private int MAP_X = default;
    private int MAP_Y = default;

    // Start is called before the first frame update

    private void Start()
    {
        //Othello_Start_Map(1);
        Piece_Set_Check(set_piece_coler);
        _uimaneger.Team_Set();
        Debug.Log(set_piece_coler);

    }


    // Update is called once per frame
    void Update()
    {
        debug_text2.text = "" + set_piece_coler;



        if (Input.GetMouseButtonDown(0) && Game_Lock == false)
        {
            Debug.Log(set_piece_coler);
            PieceSeting();
            PieceSet(set_piece_coler);
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            Map_Reset();
            Player_turn_set();
            Piece_set_ok = false;
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            Map_Reset();
            Player_turn_set();
            Piece_set_ok = false;
        }

        if (Piece_set_ok == true)
        {
            Map_Reset();
            Player_turn_set();
            Piece_set_ok = false;
        }
    }


    private void PieceSeting()
    {


        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics2D.Raycast((Vector2)ray.origin, (Vector2)ray.direction))
        {
            Hitobj = Physics2D.Raycast((Vector2)ray.origin, (Vector2)ray.direction);


            int X = (int)Hitobj.collider.gameObject.transform.position.x;
            int Y = (int)Hitobj.collider.gameObject.transform.position.y;
            if (Hit_X >= 0 && Hit_X <= MAP_X && Hit_Y >= 0 && Hit_Y <= MAP_Y)
            {
                Hit_X = X;
                Hit_Y = Y;
            }

        }

    }

    private void Piece_Make(int x, int y)
    {
        if (Now_Tiles[x, y] != (int)Piece_Color.EMPTY)
        {
            Makes(x, y);
        }
    }

    private void PieceSet(int value)
    {


        if (Now_Tiles[Hit_X, Hit_Y] == (int)Piece_Color.EFFECT)
        {
            for (int i = -1; i < 2; i++)
            {
                for (int j = -1; j < 2; j++)
                {
                    int z = Upset_num[Hit_X, Hit_Y, i + 1, j + 1];

                    if (i == 0 && j == 0)
                    {
                        continue;
                    }
                    else
                    {
                        Now_Tiles[Hit_X, Hit_Y] = value;
                        Piece_set_ok = true;
                        while (z >= 2)
                        {
                            Now_Tiles[Hit_X + i * (z - 1), Hit_Y + j * (z - 1)] = value;
                            z--;
                        }
                    }
                }
            }
        }
        else
        {
            _uimaneger.Judge();
        }

        Invoke("Reset_Text", 1);


    }

    private void Piece_Set_Check(int value)
    {
        _uimaneger.Player_turn(set_piece_coler,Trun_num);
        Upset_num = new int[Now_Tiles.GetLength(0), Now_Tiles.GetLength(1), 3, 3];

        int No_Effect_count = 0;

        for (int x = 0; x < Now_Tiles.GetLength(0); x++)
        {
            for (int y = 0; y < Now_Tiles.GetLength(1); y++)
            {
                for (int i = -1; i < 2; i++)
                {
                    for (int j = -1; j < 2; j++)
                    {
                        Upset_num[x, y, i + 1, j + 1] = 0;

                        if (i == 0 && j == 0)
                        {
                            continue;
                        }
                        else
                        {
                            int z = 2;
                            if (x + i >= 0 && y + j >= 0 &&
                                 x + i < Now_Tiles.GetLength(0) &&
                                 y + j < Now_Tiles.GetLength(1))
                            {
                                if ((Now_Tiles[x, y] == (int)Piece_Color.EMPTY || Now_Tiles[x, y] == (int)Piece_Color.EFFECT) &&
                                     Now_Tiles[x + i, y + j] != (int)Piece_Color.EMPTY &&
                                     Now_Tiles[x + i, y + j] != (int)Piece_Color.EFFECT &&
                                     Now_Tiles[x + i, y + j] != value &&
                                     Now_Tiles[x + i, y + j] != value - 2 &&
                                     Now_Tiles[x + i, y + j] != value + 2 &&
                                     x + i * z < Now_Tiles.GetLength(0) &&
                                     y + j * z < Now_Tiles.GetLength(1) &&
                                     x + i * z >= 0 && y + j * z >= 0)
                                {
                                    Debug.LogFormat("({0},{1}) x:{2} y:{3} i:{4} j:{5}", Hit_X, Hit_Y, x, y, i, j);
                                    if (Now_Tiles[x + i * z, y + j * z] != (int)Piece_Color.EMPTY && Now_Tiles[x + i * z, y + j * z] != (int)Piece_Color.EFFECT)
                                    {

                                        while (x + i * z < Now_Tiles.GetLength(0) &&
                                               y + j * z < Now_Tiles.GetLength(1) &&
                                               x + i * z >= 0 && y + j * z >= 0)
                                        {
                                            if (Now_Tiles[x + i * z, y + j * z] != (int)Piece_Color.EMPTY &&
                                                Now_Tiles[x + i * z, y + j * z] != (int)Piece_Color.EFFECT &&
                                                (value == Now_Tiles[x + i * z, y + j * z] ||
                                                value - 2 == Now_Tiles[x + i * z, y + j * z] ||
                                                value + 2 == Now_Tiles[x + i * z, y + j * z])
                                                )
                                            {
                                                Now_Tiles[x, y] = (int)Piece_Color.EFFECT;
                                                EFFECT_Make(x, y);
                                                Pass_num = 0;

                                                Upset_num[x, y, i + 1, j + 1] = z;
                                                //  Debug.LogFormat("x:{2} y:{3} i:{4} j:{5} z:{6}", Hit_X, Hit_Y, x, y, i, j,z);



                                                break;
                                            }
                                            z++;

                                        }
                                    }

                                }
                            }
                        }
                    }

                }

                if (Now_Tiles[x, y] != (int)Piece_Color.EFFECT)
                {
                    No_Effect_count++;
                }


            }
        }

        if (No_Effect_count >= Now_Tiles.GetLength(0) * Now_Tiles.GetLength(1) && Pass_num < Player_num)
        {
            Pass();
        }

        if (Pass_num >= Player_num)
        {
            GameSet_PieceCount();
        }

        for (int j = MAP_Y - 1; j >= 0; j--)
        {
            for (int i = 0; i < MAP_X; i++)
            {
                debug_text.text += Now_Tiles[i, j] + " ";
            }
            debug_text.text += "\n";
        }
    }

    private void Reset_Text()
    {
        _uimaneger.Judge_Reset();
    }

    private void Map_Reset()
    {
        //駒とエフェクトの削除
        foreach (Transform child in Parent[1].gameObject.transform)
        {
            Destroy(child.gameObject);
        }
        foreach (Transform child in Parent[2].gameObject.transform)
        {
            Destroy(child.gameObject);
        }
        Black_Piece_Value = 0;
        White_Piece_Value = 0;
        int Empty_Value = 0;

        debug_text.text = "";

        for (int x = 0; x < MAP_X; x++)
        {
            for (int y = 0; y < MAP_Y; y++)
            {
                if (Now_Tiles[x, y] == (int)Piece_Color.EFFECT)
                {
                    Now_Tiles[x, y] = 0;
                }
                Piece_Make(x, y);

                if (Now_Tiles[x, y] == (int)Piece_Color.BLACK || Now_Tiles[x, y] == (int)Piece_Color.BLACK_L)
                {
                    Black_Piece_Value++;
                }
                else if (Now_Tiles[x, y] == (int)Piece_Color.WHITE || Now_Tiles[x, y] == (int)Piece_Color.WHITE_L)
                {
                    White_Piece_Value++;
                }
                else if (Now_Tiles[x, y] == (int)Piece_Color.EMPTY)
                {
                    Empty_Value++;
                }


            }
        }

        if (Empty_Value >= Secret_num)
        {
            _uimaneger.Piece_Count(Black_Piece_Value, White_Piece_Value);
        }
        else
        {
            _uimaneger.Piece_Count_Secret();
        }
    }

    private void Map_Make(int x, int y)
    {
        Now_Tiles = new int[x, y];

        for (int i = 0; i < MAP_X; i++)
        {
            for (int j = 0; j < MAP_Y; j++)
            {
                Now_Tiles[i, j] = (int)Piece_Color.EMPTY;
                Makes(i, j);
            }
        }

    }

    public void Othello_Start_Map(int Select)
    {
        switch (Select)
        {
            case 1:
                MAP_X = 8;
                MAP_Y = 8;
                Map_Make(MAP_X, MAP_Y);
                Othello_Start_Map_01();
                break;
        }
        Map_Reset();
    }

    private void Othello_Start_Map_01()
    {
        if( set_piece_coler == 1)
        {
            Now_Tiles[3, 4] = (int)Piece_Color.BLACK;
            Now_Tiles[4, 3] = (int)Piece_Color.BLACK;
            Now_Tiles[3, 3] = (int)Piece_Color.WHITE;
            Now_Tiles[4, 4] = (int)Piece_Color.WHITE;
        }
        else if (set_piece_coler == 3)
        {
            Now_Tiles[3, 4] = (int)Piece_Color.BLACK_L;
            Now_Tiles[4, 3] = (int)Piece_Color.BLACK_S;
            Now_Tiles[3, 3] = (int)Piece_Color.WHITE_L;
            Now_Tiles[4, 4] = (int)Piece_Color.WHITE_S;
        }
    }

    private void EFFECT_Make(int x, int y)
    {
        Makes(x, y);
    }

    private void Makes(int x, int y)
    {
        int makepiece = default;
        GameObject Obj = Instantiate(Piecetype[Now_Tiles[x, y]], new Vector3(x, y, 10), new Quaternion(0, 0, 0, 0));
        switch (Now_Tiles[x, y])
        {
            case 0:
                makepiece = 0;
                break;

            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                makepiece = 1;
                break;

            case 10:
                makepiece = 2;
                break;

        }
        Obj.transform.parent = Parent[makepiece].transform;

    }

    private void GameSet_PieceCount()
    {
        _uimaneger.GameSet();
        Debug.Log("GameSet");
        Game_Set = true;
    }

    public void Player_turn_set()
    {
        Trun_num++;
        if( Trun_num == 9)
        {
            Trun_num = 1;
        }

        if (set_piece_coler <= 2)
        {
            set_piece_coler = set_piece_coler % Player_num + 1;
        }
        else
        {


            switch (Trun_num)
            {
                case 1:
                case 7:
                    set_piece_coler = 3;
                    break;
                case 2:
                case 8:
                    set_piece_coler = 6;
                    break;
                case 3:
                case 5:
                    set_piece_coler = 4;
                    break;
                case 4:
                case 6:
                    set_piece_coler = 5;
                    break;
            }
        }

        Piece_Set_Check(set_piece_coler);

    }

    private void Pass()
    {
        if (Game_Set == false)
        {
            Pass_num++;
            Map_Reset();
            Player_turn_set();
            Piece_set_ok = false;
        }
    }


}